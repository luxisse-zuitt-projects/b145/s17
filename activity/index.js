console.log('Hello from JS');

// [TASK 1:] Create a function that will prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers

function performArithmeticOperations() {
	
	let numA = parseInt(prompt('Enter your first number: '));
	let numB = parseInt(prompt('Enter your second number: '));

	let sum = numA + numB;

	// console.log(typeof numA);

	if (sum <= 9) {
		console.log('The sum of the two numbers is: ' + (numA + numB));
	} else if (sum >= 10 && sum <= 20) {
		alert('The difference of the two numbers is: ' + (numA - numB));
	} else if (sum >= 21 && sum <= 29) {
		alert('The product of the two numbers is: ' + (numA * numB));
	} else if (sum >= 30) {
		alert('The quotient of the two numbers is: ' + (numA / numB));
	}
}

// [TASK 2:] Create a function that will prompt the user for their name and age and print out different alert messages based on the user input:
  // -> If the name OR age is blank/null, print the message are you a time traveler?
 	// -> If the name AND age is not blank, print the message with the user’s name and age.

function userProfileChecker() {
	
 	let name = prompt('What is your full name: ');
	let age = parseInt(prompt('How old are you?'));


	if(name == "" || age == ""){
		alert('Are you a time traveler?');
	} else if (name == name && age == age) {
		alert("Hello " + name + "! " + "Your age is " + age);
	}
}


// TASK 3: Create a function with switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

function ageOfUser() {
	let age = prompt('How old are you?');

	switch (age) {
		case '18':
			alert('You are now allowed to party.');
			break;
		case '21':
			alert('You are now part of the adult society.');
			break;
		case '65':
			alert('We thank you for your contribution to society.');
			break;

		default:
			alert("Are you sure you're not an alien?");
	}
}

//create a function that will determine if the age is too old for preschool
function ageChecker(){
  //we are going to use a try-catch statement instead

  //get the input of the user.
  //the getElementById() will target a component within the document using its ID attribute
  //the "document" parameter describes the HTML document/file where the JS module is linked.
  // "value" => describes the value property of our elements
  let userInput = document.getElementById('age').value; 
  //alert(userInput); //checker
  //we will now target the element where we will display the output of this function.
  let message = document.getElementById('outputDisplay'); 
  console.log(typeof userInput); //string
  
  try {
  	//lets make sure that the input inserted by the user is NOT equals to a blank string.
  	//throw -> this statement examines the input and returns an error.
  	if (userInput === '' ) throw 'the input is empty';
  	//create a conditional statement that will check if the input in NOT a number 
  	//in order to check if the value is NOT A NUMBER, We will use a isNaN()
  	if (isNaN(userInput)) throw 'the input is Not a Number';
  	if (userInput <= 0) throw 'Not a valid Input' 
  	if (userInput <= 7) throw 'the input is good for preschool'; 
  	if (userInput > 7) throw 'too old for preschool';
  } catch(err) {
     //the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
     //how are we going to inject a value inside the html container?
     //=> using innerHTML property : 
     //syntax: element.innerHTML -> This will allow us to get/set the HTML markup contained within the element
     message.innerHTML = "Age Input: " + err; 
  } finally {
  	//this statement here will be executed regardless of the result above.
  	console.log('This is from the finally section');
  	//lalabas both ung block of code indicated in the finally section including the outcome of the try statement.
  }

}